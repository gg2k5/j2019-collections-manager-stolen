package by.itstep.collections.manager.entity;

public enum Role {
    ADMIN,
    USER
}
