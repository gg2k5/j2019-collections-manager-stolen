package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public List<User> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<User> foundUser = em.createNativeQuery("SELECT * FROM users", User.class).getResultList();

        em.close();
        System.out.println("Found " + foundUser.size() + " users");
        return foundUser;
    }

    @Override
    public User findById(Long id) {
        //1 : Получить экземпляр EntityManager`a
        EntityManager em = EntityManagerUtils.getEntityManager();

        //2 : Сделать что нужно
        User foundUser= em.find(User.class, id);

        //3 : Закрыть EntityManager`a
        em.close();
        System.out.println("Found users: " + foundUser);
        return foundUser;
    }

    @Override
    public User create(User users) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(users);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("User was created. Id: " + users.getId());
        return users;
    }

    @Override
    public User update(User users) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(users);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("User was updated. Id: " + users.getId());
        return users;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); // Стартуем транзикцию

        User foundUser = em.find(User.class, id);
        em.remove(foundUser); // Удаление

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
    }
}
