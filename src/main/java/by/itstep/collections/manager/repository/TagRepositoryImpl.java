package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class TagRepositoryImpl implements TagRepository {
    @Override
    public List<Tag> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Tag> foundTag = em.createNativeQuery("SELECT * FROM tags", Tag.class).getResultList();

        em.close();
        System.out.println("Found " + foundTag.size() + " tags");
        return foundTag;
    }

    @Override
    public Tag findById(Long id) {
        //1 : Получить экземпляр EntityManager`a
        EntityManager em = EntityManagerUtils.getEntityManager();

        //2 : Сделать что нужно
        Tag foundTag= em.find(Tag.class, id);

        //3 : Закрыть EntityManager`a
        em.close();
        System.out.println("Found tag: " + foundTag);
        return foundTag;
    }

    @Override
    public Tag create(Tag tag) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(tag);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("Tag was created. Id: " + tag.getId());
        return tag;
    }

    @Override
    public Tag update(Tag tag) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(tag);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("Tag was updated. Id: " + tag.getId());
        return tag;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); // Стартуем транзикцию

        Tag foundTag = em.find(Tag.class, id);
        em.remove(foundTag); // Удаление

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
    }
}
