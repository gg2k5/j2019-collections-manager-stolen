package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class CollectionRepositoryImpl implements CollectionRepository {
    @Override
    public List<Collection> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Collection> foundCollections = em.createNativeQuery("SELECT * FROM collections", Collection.class).getResultList();

        em.close();
        System.out.println("Found " + foundCollections.size() + " collections");
        return foundCollections;
    }

    @Override
    public Collection findById(Long id) {
        //1 : Получить экземпляр EntityManager`a
        EntityManager em = EntityManagerUtils.getEntityManager();

        //2 : Сделать что нужно
        Collection foundCollection= em.find(Collection.class, id);
        // foundCollection.getItems().size(); - инициализация списка
        Hibernate.initialize(foundCollection.getItems());


        //3 : Закрыть EntityManager`a
        em.close();
        System.out.println("Found collection: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Collection create(Collection collection) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(collection);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("Collection was created. Id: " + collection.getId());
        return collection;
    }

    @Override
    public Collection update(Collection collection) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(collection);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("Collection was updated. Id: " + collection.getId());
        return collection;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); // Стартуем транзикцию

        Collection foundCollection = em.find(Collection.class, id);
        em.remove(foundCollection); // Удаление

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
