package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;

import java.util.List;

public interface TagRepository {

    //1. Найти все
    List<Tag> findAll();

    //2. Найти по ID
    Tag findById(Long id);

    //3. Сохранить новый
    Tag create(Tag tag);

    //4. Обновить существующий
    Tag update(Tag tag);

    //5. Удалить по ID
    void deleteById(Long id);

}
