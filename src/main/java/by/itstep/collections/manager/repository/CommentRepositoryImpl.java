package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundComment = em.createNativeQuery("SELECT * FROM comments", Comment.class).getResultList();

        em.close();
        System.out.println("Found " + foundComment.size() + " comments");
        return foundComment;
    }

    @Override
    public Comment findById(Long id) {
        //1 : Получить экземпляр EntityManager`a
        EntityManager em = EntityManagerUtils.getEntityManager();

        //2 : Сделать что нужно
        Comment foundComment= em.find(Comment.class, id);

        //3 : Закрыть EntityManager`a
        em.close();
        System.out.println("Found comment: " + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(comment);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("Comment was created. Id: " + comment.getId());
        return comment;
    }

    @Override
    public Comment update(Comment comment) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); //Стартуем транзакцию


        //Добавление в БД
        em.persist(comment);

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
        System.out.println("Comment was updated. Id: " + comment.getId());
        return comment;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin(); // Стартуем транзикцию

        Comment foundComment = em.find(Comment.class, id);
        em.remove(foundComment); // Удаление

        em.getTransaction().commit(); // Сохраняем все, что есть
        em.close();
    }
}
