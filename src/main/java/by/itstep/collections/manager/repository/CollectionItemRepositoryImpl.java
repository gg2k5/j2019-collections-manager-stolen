package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CollectionItemRepositoryImpl implements CollectionItemRepository {
    @Override
    public List<CollectionItem> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CollectionItem> foundCollectionItems =
                em.createNativeQuery("SELECT * FROM `collection_items`", CollectionItem.class).getResultList();

        em.close();
        System.out.println("Found " + foundCollectionItems.size() + " collectionItems");
        return foundCollectionItems;
    }

    @Override
    public CollectionItem findById(Long id) {
        //1 : Получить экземпляр EntityManager`a
        EntityManager em = EntityManagerUtils.getEntityManager();

        //2 : Сделать что нужно
        CollectionItem foundCollectionItem = em.find(CollectionItem.class, id);

        //3 : Закрыть EntityManager`a
        em.close();
        System.out.println("Found collectionItems: " + foundCollectionItem);
        return foundCollectionItem;
    }

    @Override
    public CollectionItem create(CollectionItem collectionItem) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(collectionItem);

        em.getTransaction().commit();
        em.close();
        System.out.println("CollectionItem was created. Id: " + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public CollectionItem update(CollectionItem collectionItem) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(collectionItem);

        em.getTransaction().commit();
        em.close();
        System.out.println("CollectionItem was updated. Id: " + collectionItem.getId());
        return collectionItem;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CollectionItem foundCollectionItem = em.find(CollectionItem.class, id);
        em.remove(foundCollectionItem);

        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection_items").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

}
