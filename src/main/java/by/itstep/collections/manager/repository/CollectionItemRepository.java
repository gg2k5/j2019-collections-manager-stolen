package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;

import java.util.List;

public interface CollectionItemRepository {

    //1. Найти все
    List<CollectionItem> findAll();

    //2. Найти по ID
    CollectionItem findById(Long id);

    //3. Сохранить новый
    CollectionItem create(CollectionItem collectionItem);

    //4. Обновить существующий
    CollectionItem update(CollectionItem collectionItem);

    //5. Удалить по ID
    void deleteById(Long id);

    void deleteAll();
}
