package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.User;

import java.util.List;

public interface UserRepository {

    //1. Найти все
    List<User> findAll();

    //2. Найти по ID
    User findById(Long id);

    //3. Сохранить новый
    User create(User user);

    //4. Обновить существующий
    User update(User user);

    //5. Удалить по ID
    void deleteById(Long id);

}
