package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;

import java.util.List;

public interface CommentRepository {

    //1. Найти все
    List<Comment> findAll();

    //2. Найти по ID
    Comment findById(Long id);

    //3. Сохранить новый
    Comment create(Comment comment);

    //4. Обновить существующий
    Comment update(Comment comment);

    //5. Удалить по ID
    void deleteById(Long id);

}
