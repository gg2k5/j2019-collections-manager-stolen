package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ApplicationTests {

	private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
	private CollectionItemRepository itemRepository = new CollectionItemRepositoryImpl();


	@BeforeEach
	void setup() {
        itemRepository.deleteAll();
	    collectionRepository.deleteAll();



	}

	@Test
	void testCreate() {
		// given
		Collection collection = Collection.builder()
				.description("my-description")
				.imageUrl("my-image")
				.title("my-title")
				.name("my-name")
				.build();

		// when
		Collection saved = collectionRepository.create(collection);

		// then

		Assertions.assertNotNull(saved.getId()); // убеждаемся, что ID не null
	}

	@Test
	void testFindAll() {
		//given
		Collection collectionOne = Collection.builder()
				.description("my-description1")
				.imageUrl("my-image1")
				.title("my-title1")
				.name("my-name1")
				.build();

		Collection collectionTwo = Collection.builder()
				.description("my-description2")
				.imageUrl("my-image2")
				.title("my-title2")
				.name("my-name2")
				.build();
		collectionRepository.create(collectionOne);
		collectionRepository.create(collectionTwo);

		//when
		List<Collection> foundCollections = collectionRepository.findAll();

		//then
		Assertions.assertEquals(foundCollections.size(), 2);
	}

	@Test
	void save_collectionWithoutItems() {
//given
		Collection c = Collection.builder()
				.description("my-description4")
				.imageUrl("my-image4")
				.title("my-title4")
				.name("my-name4")
				.build();
//when

		Collection saved = collectionRepository.create(c);

	//then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void save_collectionItems() {

		Collection c = Collection.builder()
				.description("my-description4")
				.imageUrl("my-image4")
				.title("my-title4")
				.name("my-name4")
				.build();
//when

		Collection saved = collectionRepository.create(c);


		//given
		CollectionItem i1 = CollectionItem.builder().name("item 1").collection(saved).build();
		CollectionItem i2 = CollectionItem.builder().name("item 2").collection(saved).build();


		CollectionItem savedI1 = itemRepository.create(i1);
		CollectionItem savedI2 = itemRepository.create(i2)	;
		List<CollectionItem> items = new ArrayList<>();
		items.add(savedI1);
		items.add(savedI2);

		//then
		Assertions.assertNotNull(saved.getId());
	}


	@Test
    void findById_happyPath(){
      //given

        Collection c = Collection.builder()
                .description("my-description4")
                .imageUrl("my-image4")
                .title("my-title4")
                .name("my-name4")
                .build();


        Collection c2 = Collection.builder()
                .description("my-description44")
                .imageUrl("my-image44")
                .title("my-title44")
                .name("my-name44")
                .build();


        Collection saved = collectionRepository.create(c);
        Collection saved2 = collectionRepository.create(c2);


        CollectionItem i1 = CollectionItem.builder().name("item 1").collection(saved).build();
        CollectionItem i2 = CollectionItem.builder().name("item 2").collection(saved).build();

        CollectionItem i14 = CollectionItem.builder().name("item 1").collection(saved2).build();
        CollectionItem i24 = CollectionItem.builder().name("item 2").collection(saved2).build();
        CollectionItem i34 = CollectionItem.builder().name("item 3").collection(saved2).build();
        CollectionItem i44 = CollectionItem.builder().name("item 4").collection(saved2).build();


        itemRepository.create(i1);
        itemRepository.create(i2);
        itemRepository.create(i14);
        itemRepository.create(i24);
        itemRepository.create(i34);
        itemRepository.create(i44);

        // when
        Collection found = collectionRepository.findById(saved.getId());
        Collection found2 = collectionRepository.findById(saved2.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertNotNull(found.getId());
        Assertions.assertNotNull(found.getItems());
        Assertions.assertEquals(2, found.getItems().size());

        Assertions.assertNotNull(found2);
        Assertions.assertNotNull(found2.getId());
        Assertions.assertNotNull(found2.getItems());
        Assertions.assertEquals(4, found2.getItems().size());


	}
}
